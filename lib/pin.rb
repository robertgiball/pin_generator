class Pin
  attr_accessor :pin_number, :account_number, :sort_code, :old_pin1, :old_pin2, :old_pin3, :birthday

  # All params are optional to make testing simpler for the exercise
  def initialize(pin_number=nil, account_number=nil, sort_code=nil, old_pin1=nil, old_pin2=nil, old_pin3=nil, name=nil, birthday=nil)
    self.account_number = account_number
    self.sort_code = sort_code
    self.old_pin1 = old_pin1
    self.old_pin2 = old_pin2
    self.old_pin3 = old_pin3
    self.birthday = birthday
    self.pin_number = pin_number
    while !self.valid?
      # Generate a random number between 0 and 9999, and prepend 0s if it's below 1000
      self.pin_number = rand(9999).to_s.rjust(4, '0')
    end
  end

  # Validation methods

  # To add new rules, append the method to the following list
  def valid?
    self.valid_type &&
        self.valid_length &&
        self.valid_consecutive_digits &&
        self.valid_sequence &&
        self.valid_reuse &&
        self.valid_contents
  end

  # Check the pin number is a string
  def valid_type
    self.pin_number.is_a? String
  end

  # Check the pin number is 4 characters
  def valid_length
    self.pin_number.length == 4
  end

  # Check that there are not 3 or more consecutive identical digits
  def valid_consecutive_digits
    matches = /(.)\1{2,}/.match(self.pin_number)      # Regexp for finding 3 consecutive characters
    matches.nil?
  end

  # Check the number is not a complete ascending or descending sequence
  def valid_sequence
    seq = '0123456789'
    return false if seq.include? self.pin_number            # Check pin isn't ascending
    return false if seq.reverse.include? self.pin_number    # Check pin isn't descending
    true
  end

  # Check the pin number has not been recently used
  def valid_reuse
    !([self.old_pin1, self.old_pin2, self.old_pin3].include? self.pin_number)
  end

  # Check if the pin is contained in the account number, sort code or birthday
  def valid_contents
    return false if (!self.account_number.nil? && self.account_number.include?(self.pin_number))
    return false if (!self.sort_code.nil? && self.sort_code.include?(self.pin_number))
    return false if (!self.birthday.nil? && self.birthday.include?(self.pin_number))
    true
  end

end