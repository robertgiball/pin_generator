require "pin_generator/version"
require 'pin'

module PinGenerator

  # All params are optional to make testing simpler for the exercise
  def self.generate_pin(account_number=nil, sort_code=nil, old_pin1=nil, old_pin2=nil, old_pin3=nil, name=nil, bday=nil)
    Pin.new(nil, account_number, sort_code, old_pin1, old_pin2, old_pin3, nil, nil).pin_number
  end

end
