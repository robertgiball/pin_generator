# PinGenerator

Gem to implement a basic pin generator for the QuantBet tech test.

The solution is implemented as a Ruby Gem, with the idea that it could simply be installed as part of a larger project, and be developed seperately to the main codebase. All of the tests are writen in rspec, and can be ran with `rspec spec/pin_spec.rb`. 

The additional rule checks to see if the pin number is contained within the birthday. To add this feature was just one line of code (line 64 in `lib/pin.rb`)

## Usage

After checking out the repo, run `bin/setup` to install dependencies. You can either install the gem and use it in any ruby app or console, or you can also run `bin/console` for an interactive prompt that will allow you to experiment.

To generate a pin number, you can run the following from inside a interactive prompt:

`PinGenerator.generate_pin`

Or if you want to test things such as making sure old pins are avoided, you can pass params such as:

`PinGenerator.generate_pin(account_number='13561342', sort_code='711313', old_pin1='4364', old_pin2='2464', old_pin3='5423', bday='25031991')`

In it's current state the app doesn't require the parameters to be empted, but changing this would be extremely simple.