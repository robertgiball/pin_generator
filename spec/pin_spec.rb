require 'spec_helper'
describe Pin do

  let(:pin) { Pin.new('1245') }

  context 'when a valid pin number is generated' do
    it 'should call all validations in order' do
      expect(pin).to receive(:valid_type).and_return(true).ordered
      expect(pin).to receive(:valid_length).and_return(true).ordered
      expect(pin).to receive(:valid_consecutive_digits).and_return(true).ordered
      expect(pin).to receive(:valid_sequence).and_return(true).ordered
      expect(pin).to receive(:valid_reuse).and_return(true).ordered
      expect(pin).to receive(:valid_contents).and_return(true).ordered

      pin.valid?
    end
  end

  describe 'validations' do

    describe 'valid_type' do

      context 'when pin number is a string' do
        it 'should return true' do
          expect(pin.valid_type).to be true
        end
      end

      context 'when any other type is generated' do
        it 'should return false' do
          pin.pin_number = 123
          expect(pin.valid_type).to be false
          pin.pin_number = nil
          expect(pin.valid_type).to be false
        end
      end

    end

    describe 'valid_length' do

      context 'when the pin number is the correct length' do
        it 'should return true' do
          pin.pin_number = '1234'
          expect(pin.valid_length).to be true
        end
      end

      context 'when the pin number is an incorrect length' do
        it 'should return false' do
          pin.pin_number = '123'
          expect(pin.valid_length).to be false
        end
      end

    end

    describe 'valid_consecutive_digits' do

      context 'when the pin number has less than 3 consecutive digits' do
        it 'should return true' do
          expect(pin.valid_consecutive_digits).to be true
        end
      end

      context 'when the pin number has more than 2 consecutive digits' do
        it 'should return false' do
          pin.pin_number = '1112'
          expect(pin.valid_consecutive_digits).to be false
        end
      end

    end

    describe 'valid_sequence' do

      context 'when the pin number is not a complete number sequence' do
        it 'should return true' do
          expect(pin.valid_sequence).to be true
        end
      end

      context 'when the pin number is a complete ascending number sequence' do
        it 'should return false' do
          pin.pin_number = '1234'
          expect(pin.valid_sequence).to be false
        end
      end

      context 'when the pin number is a complete descending number sequence' do
        it 'should return false' do
          pin.pin_number = '9876'
          expect(pin.valid_sequence).to be false
        end
      end

    end

    describe 'valid_reuse' do

      before do
        pin.old_pin1 = '1456'
        pin.old_pin2 = '0984'
        pin.old_pin3 = '3464'
      end

      context 'when the pin number does not match the old pins' do
        it 'should return true' do
          expect(pin.valid_reuse).to be true
        end
      end

      context 'when the pin number matches an old pin' do
        it 'should return false' do
          pin.pin_number = '1456'
          expect(pin.valid_reuse).to be false
        end
      end

    end

    describe 'valid_contents' do

      before do
        pin.account_number = '13561342'
        pin.sort_code = '711313'
        pin.birthday = '23051991'
      end

      context 'when the pin number is not contained in the account number, sort code, or birthday' do
        it 'should return true' do
          expect(pin.valid_contents).to be true
        end
      end

      context 'when the pin number is contained in the account number, sort code or birthday' do
        it 'should return false' do
          pin.pin_number = '7113'                 # Check pin in sort code is rejected
          expect(pin.valid_contents).to be false
          pin.pin_number = '1356'                 # Check pin in account number is rejected
          expect(pin.valid_contents).to be false
          pin.pin_number = '2305'                 # Check pin in bday is rejected
          expect(pin.valid_contents).to be false
        end
      end

    end

  end

end
